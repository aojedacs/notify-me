/*
 * Copyright (c) Microsoft Corporation. All rights reserved. Licensed under the MIT license.
 * See LICENSE in the project root for license information.
 */
const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
  apiKey: 'AIzaSyAX96QaYKLTPSuy3TthJOM4bAS3Ounzzi8',
  authDomain: 'notifyme-739e4.firebaseapp.com',
  projectId: 'notifyme-739e4'
});




/* global global, Office, self, window */
Office.onReady(() => {
  // If needed, Office.js is ready to be called
});

/**
 * Shows a notification when the add-in command is executed.
 * @param event {Office.AddinCommands.Event}
 */
function action(event) {
  const message = {
    type: Office.MailboxEnums.ItemNotificationMessageType.InformationalMessage,
    message: "Voice notification has been created",
    icon: "Icon.80x80",
    persistent: true
  };

  // var db = firebase.firestore();

  // var events = db.collection("eventdetails");  

  // db.collection("eventdetails").where("email", "==", "angel.ojeda@foundersworkshop.com")
  //   .get()
  //   .then(function (querySnapshot) {
  //     querySnapshot.forEach(function (doc) {
  //       // doc.data() is never undefined for query doc snapshots
  //       console.log(doc.id, " => ", doc.data());
  //     });
  //   })
  //   .catch(function (error) {
  //     console.log("Error getting documents: ", error);
  //   });



  var item = Office.context.mailbox.item;

  //  console.log(JSON.stringify(item.subject))

  // const _item = JSON.parse(JSON.stringify(item._data$p$0._data$p$0))

  // console.log("data", _item)
  // const apointment = {
  //   meetingId: _item.conversationId,
  //   to: _item.to,
  //   title: _item.subject,
  //   startDate: _item.start,
  //   endDate: _item.end,
  //   where: _item.location.contains("https://zoom.us") ? "Zoom" : _item.location,
  //   zoomUrl: _item.location.contains("https://zoom.us") ? _item.location : ""
  // }

  // const adsas = {
  //   id: _item.conversationId,
  //   myEmail: _item.userEmailAddress,
  //   from: _item.from,
  //   subject: _item.subject,
  //   start: _item.start,
  //   end: _item.end,
  //   to: _item.to,
  //   location: _item.location
  // }

  // if (_item.location.contains("https://zoom.us"))

    // {
    //   email: "angel.ojeda@foundersworkshop.com",
    //   name:"Angel Ojeda",
    //   apointments:[

    //   ]
    // }

    // console.log(JSON.stringify(scheduleData))

  // writeUserData(scheduleData)

  // Show a notification message
  Office.context.mailbox.item.notificationMessages.replaceAsync("action", message);

  // Be sure to indicate when the add-in command function is complete
  event.completed();
}

function writeUserData(scheduleData) {
  firebase.database().ref('appointment').set({ scheduleData }, function (error, result) {
    if (error) {
      // The write failed...
      console.log('error', error)
    } else {
      // Data saved successfully!
      console.log('result', result)
    }
  });

}

function getGlobal() {
  return typeof self !== "undefined"
    ? self
    : typeof window !== "undefined"
      ? window
      : typeof global !== "undefined"
        ? global
        : undefined;
}

const g = getGlobal();

// the add-in command functions need to be available in global scope
g.action = action;
